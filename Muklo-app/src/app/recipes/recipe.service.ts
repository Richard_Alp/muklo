import {EventEmitter, Injectable} from '@angular/core';
import {Recipe} from './recipe.model';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();


  private recipes: Recipe[] = [
    new Recipe(
      'GARLIC BUTTER SHRIMP WITH ASPARAGUS',
      'So much flavor and so easy to throw together, this shrimp and asparagus skillet dinner is a winner!',
      'https://www.eatwell101.com/wp-content/uploads/2018/10/Lemon-Garlic-Butter-Shrimp-with-Asparagus-2-1.jpg',
      [
        new Ingredient('medium raw shrimp', 15),
        new Ingredient('asparagus (1 bunch) rinsed and trimmed', 12)
      ]),
    new Recipe(
      'GARLIC BUTTER CHICKEN BITES WITH LEMON ASPARAGUS',
      'So much flavor and so easy to throw together, this chicken and asparagus recipe is a winner for dinnertime!',
      'https://www.eatwell101.com/wp-content/uploads/2019/04/chicken-and-asparagus-skillet-recipe-2.jpg',
      [
        new Ingredient('boneless, skinless chicken breasts', 4),
        new Ingredient('asparagus (2 bunch) rinsed and trimmed', 22)
      ])
  ];

  constructor(private slService: ShoppingListService) {}

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }
}
