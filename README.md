# Muklo
## Muklo is a Shopping List & Recipe Book

### Planning the Muklo App:

- Root (C)
  - Header (C)
    - Shopping List (F)
      - Shopping List (S)
      - Shopping List (C)
      - Shopping List Edit (C)
      - Ingredient(M)
    - Recipe Book (F)
      - Recipe (S)
      - Recipe list (C)
      - Recipe Item (C)
      - Recipe Detail (C)
      - Recipe(M)
      
Component (C) | Feature (F) | Model (M) | Service (S)
